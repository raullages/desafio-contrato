# Contratos

Este projeto simula um controle de contratos e de partes.
O projeto ele foi desenvolvido usando React para Frontend e Json-server para Backend.

# Iniciando o projeto

## Frontend
### `O Frontend irá rodar na porta 8000`
    git clone https://gitlab.com/raullages/desafio-contrato.git
    cd desafio-contrato
    npm install
    yarn start

## Backend com json-server
### `O Backend irá rodar na porta 8001`
    Abra o terminal na pasta api (Que esta na raiz do projeto)
    npm install
    npm start
