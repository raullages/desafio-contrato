import React, { useState, useEffect } from 'react'
import './modal.css'

const Modal = (props) => {

  const [visible, setVisible] = useState(false)
  const [width, setWidth] = useState('60%')

  useEffect(() => {
    setVisible(props.modalShow)
    setWidth(props.width)
  }, [props.modalShow, props.width])

  return (
    <div>
      {
        visible &&
          <div className="container-bg">
            <div
              className="container-modal"
              style={{'width': width}}
            >
              <div className="container-modal-title">
                <h4 className="modal-title"> {props.title}</h4>
                <button
                  type="button"
                  className="close"
                  data-dismiss="modal"
                  aria-label="Close"
                  onClick={ _ => props.modalHide(false) }
                >
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <hr/>
              <div>
                { props.children }
              </div>
            </div>
          </div>
      }
    </div>
  )
}

export default Modal