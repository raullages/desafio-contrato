 import React from 'react'
 import { Switch, Route, Redirect } from 'react-router'

 import ctContratos from '../views/contratos/main'
 import ctPartes from '../views/partes/main'

export default props => (
  <Switch>
    <Route path='/Contratos' component={ctContratos} exact />
    <Route path='/Partes' component={ctPartes} />
    <Redirect from='*' to='/Contratos' />
  </Switch>
)