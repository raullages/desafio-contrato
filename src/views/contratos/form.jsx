import React, { useState, useEffect, useReducer } from 'react'
import './main.css'
import axios from 'axios'

const form = {
  id: 0,
  titulo: '',
  dataInicio: '',
  vencimento: '',
  arquivo: '',
  partesRelacionadas: []
}

const optionDefault = ''
const baseUrl = 'http://localhost:8001/'

const FormContrato = (props) => {
  const [partesRelacionadas, setPartesRelacionadas] = useState([])
  const [partes, setPartes]= useState({})
  const [state, dispatch] = useReducer(reducer, form)

  function reducer(state, action) {
    const { name, payload } = action
    switch(action.type) {
      case 'ON_CHANGE':
        return { ...state, [name] : payload }
      case 'ON_CLEAR':
        return form
      case 'ADD_PARTE':
        return { ...state, partesRelacionadas: action.payload }
      case 'REMOVE_PARTE':
        return { ...state, partesRelacionadas: action.payload }
      default:
        return state
    }
  }

  useEffect(() => {
    async function fetchPartes() {
      const response = await axios(`${baseUrl}partes`).then(resp => resp.data)
      setPartesRelacionadas(response)
    }
    fetchPartes()
  }, [])

  function upload(e) {
    const formData = new FormData()
    const file = e.target.files

    formData.append("file", file[0])

    axios.post('https://file.io/?expires=1w', formData).then(resp => {
      dispatch({
        type: 'ON_CHANGE',
        name: 'arquivo',
        payload: {
          name: file[0].name,
          link: resp.data.link
        }
      })
    }).catch(err => {
      console.log(err)
    })
  }
  
  function dispatchFillFields(e) {
    dispatch({
      type: 'ON_CHANGE',
      name: e.target.name,
      payload: e.target.value
    })
  }

  function dispatchNewItem() {
    if (Object.keys(partes).length > 0) {
      const obj = partesRelacionadas.filter(o => o.id === Number(partes))[0]

      dispatch({
        type: 'ADD_PARTE',
        payload: [...state.partesRelacionadas, obj]
      })
      setPartes({})
    }
  }

  function dispatchRemoveItem(index) {
    state.partesRelacionadas.splice(index, 1)

    dispatch({
      type: 'REMOVE_PARTE',
      payload: [...state.partesRelacionadas]
    })
  }

  function clickSalvar() {
    const type = form.id !== 0 ? 'put' : 'post'

    axios[type](`${baseUrl}contratos`, state).then((resp) => {
      props.clickSalvar(false)
    })
  }

  function labelInputFile() {
    return state.arquivo.name
  }

  function renderForm() {
    return (
      <div className="form mt-3">
        <div className="row">
          <div className="col-12 col-md-12">
            <div className="form-group">
              <label>Título:</label>
              <input
                className="form-control"
                type="text"
                name="titulo"
                value={state.titulo}
                onChange={dispatchFillFields}
              />
            </div>
          </div>
          <div className="col-12 col-md-6">
            <div className="form-group">
              <label>Data Início:</label>
              <input
                className="form-control"
                type="date"
                name="dataInicio"
                value={state.dataInicio}
                onChange={dispatchFillFields}
              />
            </div>
          </div>
          <div className="col-12 col-md-6">
            <div className="form-group">
              <label>Data Vencimento:</label>
              <input
                className="form-control"
                type="date"
                name="vencimento"
                value={state.vencimento}
                onChange={dispatchFillFields}
              />
            </div>
          </div>
          <div className="col-12 col-md-12">
            <label>Adicionar Partes:</label>
            <div className="input-group">
              <select
                multiple={false}
                className="custom-select"
                name="partesRelacionadas"
                value={partes}
                onChange={e => setPartes(e.target.value)}
              >
                <option value={optionDefault}>Escolha</option>
                {
                  partesRelacionadas.map((o) => (
                    <option key={o.id} value={o.id}>{`${o.nome} ${o.sobrenome}`}</option>
                  ))
                }
              </select>
              <div className="input-group-append">
                <button
                  className="btn btn-outline-primary"
                  type="button"
                  onClick={ () => dispatchNewItem()}
                >
                  Add
                </button>
              </div>
            </div>
          </div>
          <div className="col-12 col-md-12 mt-3">
            {     
              state.partesRelacionadas.length > 0 ?
              <div className="row">
                {
                  state.partesRelacionadas.map((o, index) => (
                    <div
                      className="col-12 col-sm-4 col-md-3 form-group"
                      key={index}
                    >
                      <div className="col-12 cardPartes">
                        <span>{`${o.nome} ${o.sobrenome}`}</span>
                        <i
                          title="Clique para remover"
                          className="float-right uil uil-times"
                          onClick={() => dispatchRemoveItem(index)}
                        >
                        </i>
                      </div>
                    </div>
                  ))
                }
              </div>
              : <span className="text-primary mt-2">Nenhuma parte adicionada.</span>
            }
          </div>
          <div className="col-12 col-md-12 mt-4">
            <div className="form-group custom-file">
              <label className="custom-file-label">
                { labelInputFile() }
              </label>
              <input
                type="file"
                id="customFile"
                className="custom-file-input"
                onChange={e => upload(e)}
                disabled={state.arquivo.name !== undefined}
              />
            </div>
            {
              state.arquivo.name !== undefined ?
              <div className="mt-3">
                <span>Contrato:</span>
                <div className="col-12 col-sm-4 mt-2 form-group cardFile">
                  <span>
                    {state.arquivo.name}
                    <i
                      className="float-right uil uil-trash-alt text-danger"
                      onClick={() => dispatch({type: 'ON_CHANGE', name: 'arquivo', payload: {}})}
                    ></i>
                  </span>
                </div>
              </div>
              : ''
            }
          </div>
          <div className="col-12 mt-5">
            <div className="form-group border-top">
              <button
                className="btn col-12 col-md-3 float-right btn-outline-success mt-2"
                onClick={e => clickSalvar(e)}
              >
                <i className="uil uil-check-circle"></i>
                Salvar
              </button>
              <button
                className="btn col-12 col-md-3 btn-light mt-2"
                onClick={e => dispatch({type: 'ON_CLEAR'})}
              >
                Limpar
              </button>
            </div>
          </div>
        </div>
      </div>
    )
  }
  
  return (
    <div>
      { renderForm() }
    </div>
  )
}

export default FormContrato