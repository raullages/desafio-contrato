import React, { useState, useEffect } from 'react'
import axios from 'axios'
import Modal from './../../components/modal/main'
import Form from './form'

const baseUrl = 'http://localhost:8001'

function Contratos() {
  const [listContratos, setListContratos] = useState([])
  const [isOpen, setIsOpen] = useState(false)

  useEffect(() => {
    fetchContratos()
  }, [])

  async function fetchContratos() {
    const response = await axios(`${baseUrl}/contratos`).then(resp => resp.data)
    setListContratos(response)
  }

  function renderButtonAdd() {
    return (
      <div className="col-12">
        <button
          className="col-12 col-md-2 float-right btn btn-outline-primary mb-3 mt-3"
          onClick={_ => modalShow()}
        >
          <i className="uil uil-plus"></i>
          Adicionar
        </button>    
      </div>
    )
  }

  function renderTable() {
    return (
      <div className="row">
        { renderButtonAdd() }
        <div className="col-12 table-responsive">
          <table className="table table-striped table-borderless table-condensed">
            <thead>
              <tr>
                <th>Titulo</th>
                <th>Data Inicio</th>
                <th>Vencimento</th>
                <th>Partes Relacionadas</th>
                <th>Arquivo</th>
                <th></th>
                <th></th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              { renderRows() }
            </tbody>
          </table>
        </div>
      </div>
    )
  }
  
  function renderRows() {
    return listContratos.map(item => (
      <tr key={item.id}>
        <td style={{'verticalAlign': 'middle'}}>{item.titulo}</td>
        <td style={{'verticalAlign': 'middle'}}>{item.dataInicio}</td>
        <td style={{'verticalAlign': 'middle'}}>{item.vencimento}</td>
        <td style={{'verticalAlign': 'middle'}}>
          {
            item.partesRelacionadas.map(it => (
              <h6 key={it.id}>
                {`${it.nome} ${it.sobrenome}`}
              </h6>
            ))
          }
        </td>
        <td style={{'verticalAlign': 'middle'}}>
          <a
            href={item.arquivo.link}
            target="_blank"
          >
            {item.arquivo.name}
          </a>
        </td>
        <td style={{'verticalAlign': 'middle'}} className="text-center">
          <button className="btn btn-link text-info">
            <i className="uil uil-search"></i>
          </button>
        </td>
        <td style={{'verticalAlign': 'middle'}} className="text-center">
          <button
            className="btn btn-link text-primary"
            onClick={() => clickEdit(item)}
          >
            <i className="uil uil-pen"></i>
          </button>
        </td>
        <td style={{'verticalAlign': 'middle'}} className="text-center">
          <button
            className="btn btn-link text-danger"
            onClick={() => clickRemove(item)}
          >
            <i className="uil uil-trash-alt"></i>
          </button>
        </td>
      </tr>
    ))
  }

  function clickRemove(item) {
    axios.delete(`${baseUrl}/contratos/${item.id}`).then(resp => {
      fetchContratos()
    })
  }

  function clickEdit(item) {
    console.log(item)
  }

  function modalShow() {
    setIsOpen(true)
  }

  function modalHide(e) {
    setIsOpen(e)
    fetchContratos()
  }

  return (
    <div>
      { renderTable() }
      <div>
        <Modal
          title="Adicionar Contrato"
          width="60%"
          modalShow={isOpen}
          modalHide={modalHide}
        >
          <Form
            clickSalvar={modalHide}
          />
        </Modal>
      </div>
    </div>
  )
}

export default Contratos