import React, { useState } from 'react'
import { BrowserRouter, Link } from 'react-router-dom'
import Routes from './../routers/main'

export default () => {
  const [active, setActive] = useState('contratos')

  return (
    <BrowserRouter>
      <div className="container-fluid" style={{'backgroundColor': '#2196f311', 'height' : '100vh'}}>
        <div className="row">
          <div className="col-12 col-md-10 offset-md-1">
            <div className="row">
              <div className="col-12 text-center text-primary p-3">
                <h1>Contratos</h1>
              </div>
              <div className="col-12 mt-1">
                <ul className="nav nav-tabs" style={{'backgroundColor' : '#FFF'}}>
                  <li className="nav-item">
                    <Link
                      onClick={() => setActive('contratos')}
                      className={active === 'contratos' ? 'nav-link active' : 'nav-link'}
                      to="/Contratos"
                    >
                      Contratos
                    </Link>
                  </li>
                  <li className="nav-item">
                    <Link
                      onClick={() => setActive('partes')}
                      className={active === 'partes' ? 'nav-link active' : 'nav-link'}
                      to="/Partes"
                    >
                      Partes
                    </Link>
                  </li>
                </ul>
              </div>
              <div className="col-12" style={{'backgroundColor' : '#FFF', 'height' : '500px'}}>
                <Routes />
              </div>
            </div>
          </div>
        </div>
      </div>
    </BrowserRouter>
  )
}
