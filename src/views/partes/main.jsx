import React, { useState, useEffect } from 'react'
import axios from 'axios'
import Modal from './../../components/modal/main'
import FormPartes from './form'

const baseUrl = 'http://localhost:8001'

function Partes() {
  const [listPartes, setListPartes] = useState([])
  const [isOpen, setIsOpen] = useState(false)

  useEffect(() => {
    fetchList()
  }, [])

  async function fetchList() {
    const response = await axios(`${baseUrl}/partes`).then(resp => resp.data)
    setListPartes(response)
  }

  function renderButtonAdd() {
    return (
      <div className="col-12">
        <button
          className="col-12 col-md-2 float-right btn btn-outline-primary mb-3 mt-3"
          onClick={_ => modalShow()}
        >
          <i className="uil uil-plus"></i> Adicionar
        </button>    
      </div>
    )
  }

  function renderTable() {
    return (
      <div className="row">
        { renderButtonAdd() }
        <div className="col-12 table-responsive">
          <table className="table table-striped table-condensed">
            <thead>
              <tr>
                <th>Nome</th>
                <th>Sobrenome</th>
                <th>CPF</th>
                <th>Email</th>
                <th>Telefone</th>
                <th></th>
                <th></th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              { renderRows() }
            </tbody>
          </table>
        </div>
      </div>
    )
  }

  function renderRows() {
    return listPartes.map(item => (
      <tr key={item.id}>
        <td style={{'verticalAlign': 'middle'}}>{item.nome}</td>
        <td style={{'verticalAlign': 'middle'}}>{item.sobrenome}</td>
        <td style={{'verticalAlign': 'middle'}}>{item.cpf}</td>
        <td style={{'verticalAlign': 'middle'}}>{item.email}</td>
        <td style={{'verticalAlign': 'middle'}}>{item.telefone}</td>
        <td className="text-right">
          <button className="btn btn-link text-info">
            <i className="uil uil-search"></i>
          </button>
        </td>
        <td className="text-center">
          <button className="btn btn-link text-primary">
            <i className="uil uil-pen"></i>
          </button>
        </td>
        <td className="text-left">
          <button
            className="btn btn-link text-danger"
            onClick={() => clickRemove(item)}
          >
            <i className="uil uil-trash-alt"></i>
          </button>
        </td>
      </tr>
    ))
  }

  function modalShow() {
    setIsOpen(true)
  }

  function modalHide(event) {
    setIsOpen(event)
  }

  function clickSalvar() {
    setIsOpen(false)
    fetchList()
  }

  function clickRemove(item) {
    axios.delete(`${baseUrl}/partes/${item.id}`).then(resp => {
      fetchList()
    })
  }
  
  return (
    <div>
      { renderTable() }    
      <Modal
        title="Adicionar Partes"
        width="60%"
        modalShow={isOpen}
        modalHide={modalHide}
      >
        <FormPartes
          clickSalvar={clickSalvar}
        />
      </Modal>
    </div>
  )
}

export default Partes