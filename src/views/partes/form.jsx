import React, { useReducer } from 'react'
import axios from 'axios'

const form = {
  id: 0,
  nome: '',
  sobrenome: '',
  email: '',
  cpf: '',
  telefone: ''
}
const baseUrl = 'http://localhost:8001'

const FormPartes = (props) => {
  const [state, dispatch] = useReducer(reducer, form)

  function reducer(state, action) {
    const { name, payload } = action
    switch(action.type) {
      case 'ON_CHANGE':
        return { ...state, [name] : payload }
      case 'ON_CLEAR':
        return form
      case 'ADD_PARTE':
        return { ...state, partesRelacionadas: action.payload }
      default:
        return state
    }
  }

  function dispatchFillFields(e) {
    dispatch({
      type: 'ON_CHANGE',
      name: e.target.name,
      payload: e.target.value
    })
  }

  function clickSalvar() {
    const type = form.id !== 0 ? 'put' : 'post'

    axios[type](`${baseUrl}/partes`, state).then((resp) => {
      props.clickSalvar()
    })
  }

  function renderForm() {
    return (
      <div className="form mt-3">
        <div className="row">
          <div className="col-12 col-md-12">
            <div className="form-group">
              <label>Nome:</label>
              <input
                className="form-control"
                type="text"
                name="nome"
                value={state.nome}
                onChange={dispatchFillFields}
              />
            </div>
          </div>
          <div className="col-12 col-md-6">
            <div className="form-group">
              <label>Sobrenome:</label>
              <input
                className="form-control"
                type="text"
                name="sobrenome"
                value={state.sobrenome}
                onChange={dispatchFillFields}
              />
            </div>
          </div>
          <div className="col-12 col-md-6">
            <div className="form-group">
              <label>CPF:</label>
              <input
                className="form-control"
                type="text"
                name="cpf"
                value={state.cpf}
                onChange={dispatchFillFields}
              />
            </div>
          </div>
          <div className="col-12 col-md-6">
            <div className="form-group">
              <label>Email:</label>
              <input
                className="form-control"
                type="text"
                name="email"
                value={state.email}
                onChange={dispatchFillFields}
              />
            </div>
          </div>
          <div className="col-12 col-md-6">
            <div className="form-group">
              <label>Telefone:</label>
              <input
                className="form-control"
                type="text"
                name="telefone"
                value={state.telefone}
                onChange={dispatchFillFields}
              />
            </div>
          </div>
          <div className="col-12 mt-5">
            <div className="form-group border-top">
              <button
                className="btn col-12 col-md-3 float-right btn-outline-success mt-2"
                onClick={e => clickSalvar(e)}
              >
                <i className="uil uil-check-circle"></i>
                Salvar
              </button>
              <button
                className="btn col-12 col-md-3 btn-light mt-2"
                onClick={e => dispatch({type: 'ON_CLEAR'})}
              >
                Limpar
              </button>
            </div>
          </div>
        </div>
      </div>
    )
  }

  return (
    <div>
      { renderForm() }
    </div>
  )
}

export default FormPartes